class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :destroy]
  before_action :proper_user, only: :destroy
  
  def new
    @micropost = current_user.microposts.build if logged_in?
  end
  
  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "投稿を作成しました"
      redirect_to "/microposts/#{@micropost.id}"
    else
      @feed_items = []
      render "static_pages/home"
    end
  end
  
  def show
    @micropost = Micropost.find_by(id: params[:id])
  end
  
  def destroy
    @micropost = Micropost.find_by(id: params[:id])
    @micropost.destroy
    flash[:success] = "削除しました"
    redirect_to "/"
  end
  
  private
    
    def micropost_params
      params.require(:micropost).permit(:content, :picture)
    end
    
    def proper_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
end
