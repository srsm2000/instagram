class StaticPagesController < ApplicationController
  def home
    @url = request.url
    if logged_in?
      @micropost = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end
  
  def term_of_service
  end

end
