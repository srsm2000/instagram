Rails.application.routes.draw do
  
  root :to => 'sessions#new'
  get 'term-of-service', to: 'static_pages#term_of_service'
  get '/password/:id', to: 'password_edit#edit'
  get '/home',    to: 'static_pages#home'

  get '/signup', to: 'users#new'
  post '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :password_edit, only:[:edit, :update]
  resources :microposts, only: [:new, :show, :create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
