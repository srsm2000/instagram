class ChangeUsersModel < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :web_site, :string
    add_column :users, :introduce, :text
    add_column :users, :phone, :string
    add_column :users, :gender, :integer
  end
end
